package com.cng.elasticsetup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cng.elasticsetup.service.KafkaService;

@Controller
@RequestMapping("/kafka")
public class KafkaController {

    
    @Autowired
    private KafkaService kafkaService;

    @GetMapping("/send_log")
    @ResponseBody
    public String send() {
        String logString = "{\r\n"
    			+ "  \"logid\": \"7e7b806a-fd93-413f-963a-5dd435f7065a\",\r\n"
    			+ "  \"receiveloginfo\": {\r\n"
    			+ "    \"receiveyear\": 2023,\r\n"
    			+ "    \"receivedayinyear\": 156,\r\n"
    			+ "    \"receivemonth\": 6,\r\n"
    			+ "    \"receivedayinmonth\": 5,\r\n"
    			+ "    \"receivedayinweek\": 1,\r\n"
    			+ "    \"receivehour\": 8,\r\n"
    			+ "    \"receiveminute\": 21,\r\n"
    			+ "    \"receivesecond\": 29,\r\n"
    			+ "    \"receivemillisecond\": 429,\r\n"
    			+ "    \"receivems\": 1685953289429,\r\n"
    			+ "    \"receivezone\": 28800000,\r\n"
    			+ "    \"receivedate\": \"2023-06-05T08:21:29.429+0000\",\r\n"
    			+ "    \"couip\": \"192.168.100.254\",\r\n"
    			+ "    \"couname\": \"gateway\",\r\n"
    			+ "    \"tenantname\": \"D0034\",\r\n"
    			+ "    \"colip\": [\r\n"
    			+ "      \"192.168.100.202\",\r\n"
    			+ "      \"192.168.100.201\",\r\n"
    			+ "      \"192.168.100.203\"\r\n"
    			+ "    ],\r\n"
    			+ "    \"colport\": 5140,\r\n"
    			+ "    \"colname\": \"clientlog01.dev.tokyo.maximusholding.com\",\r\n"
    			+ "    \"parselogstatus\": \"success\",\r\n"
    			+ "    \"parserid\": \"1010\"\r\n"
    			+ "  },\r\n"
    			+ "  \"rawdata\": {\r\n"
    			+ "    \"rawmsg\": \"<189>date=2023-06-05 time=16:21:29 devname=\\\"mxgw\\\" devid=\\\"FWF60FTK20005847\\\" eventtime=1685953288663860979 tz=\\\"+0800\\\" logid=\\\"0000000013\\\" type=\\\"traffic\\\" subtype=\\\"forward\\\" level=\\\"notice\\\" vd=\\\"root\\\" srcip=198.235.24.53 srcport=51543 srcintf=\\\"wan1\\\" srcintfrole=\\\"wan\\\" dstip=116.92.193.43 dstport=2080 dstintf=\\\"wan1\\\" dstintfrole=\\\"wan\\\" srccountry=\\\"Canada\\\" dstcountry=\\\"Hong Kong\\\" sessionid=215210845 proto=6 action=\\\"deny\\\" policyid=0 policytype=\\\"policy\\\" service=\\\"tcp/2080\\\" trandisp=\\\"noop\\\" duration=0 sentbyte=0 rcvdbyte=0 sentpkt=0 rcvdpkt=0 appcat=\\\"unscanned\\\" crscore=30 craction=131072 crlevel=\\\"high\\\"\",\r\n"
    			+ "    \"sha256hash\": \"8a1c84c52ba19bbcb6438c7814b4b20a951ccf08b6f623dc856389e30936849f\"\r\n"
    			+ "  },\r\n"
    			+ "  \"parsedlog\": {\r\n"
    			+ "    \"msg\": \"Traffic forward message from 198.235.24.53 to 116.92.193.43 \",\r\n"
    			+ "    \"parsertype\": \"syslog\",\r\n"
    			+ "    \"srcip\": \"198.235.24.53\",\r\n"
    			+ "    \"taxoutobj\": \"11\",\r\n"
    			+ "    \"trgip\": \"116.92.193.43\",\r\n"
    			+ "    \"parsedtime\": \"1685953289431\",\r\n"
    			+ "    \"blsourceservice\": \"tcp/51543/-\",\r\n"
    			+ "    \"taxoutcon\": \"10\",\r\n"
    			+ "    \"srccountry\": \"CAN\",\r\n"
    			+ "    \"trgport\": \"2080\",\r\n"
    			+ "    \"taxevtobj\": \"4\",\r\n"
    			+ "    \"prot\": \"tcp\",\r\n"
    			+ "    \"einfo\": \"{\\\"Virtual Domain\\\":\\\"root\\\",\\\"Device ID\\\":\\\"\\\",\\\"action\\\":\\\"deny\\\",\\\"Log ID\\\":\\\"0000000013\\\"}\",\r\n"
    			+ "    \"bltargetservice\": \"tcp/2080/-\",\r\n"
    			+ "    \"taxoutpro\": \"3\",\r\n"
    			+ "    \"mappedseverity\": \"0\",\r\n"
    			+ "    \"obsname\": \"mxgw\",\r\n"
    			+ "    \"srcuname\": \"root\",\r\n"
    			+ "    \"blsrc_srcip\": \"{\\\"UCE\\\":80,\\\"Dshield-block\\\":100}\",\r\n"
    			+ "    \"evt\": \"Traffic forward message\",\r\n"
    			+ "    \"taxevtcon\": \"5\",\r\n"
    			+ "    \"pname\": \"Fortinet Fortigate\",\r\n"
    			+ "    \"taxevtst\": \"3\",\r\n"
    			+ "    \"parserver\": \"1.0\",\r\n"
    			+ "    \"taxevtact\": \"20\",\r\n"
    			+ "    \"polid\": \"0\",\r\n"
    			+ "    \"srcport\": \"51543\",\r\n"
    			+ "    \"trgcountry\": \"HKG\",\r\n"
    			+ "    \"blscore_srcip\": \"180\"\r\n"
    			+ "  }\r\n"
    			+ "}";
        kafkaService.sendMessage("parsedLog", logString);
        return "ok";
    }   
}
