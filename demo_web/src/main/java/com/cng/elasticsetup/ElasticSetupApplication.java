package com.cng.elasticsetup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.cng.elasticsetup")
public class ElasticSetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElasticSetupApplication.class, args);
	}

}
